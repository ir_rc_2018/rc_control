import math
import numpy as np

from scipy import spatial
import matplotlib.pyplot as plt


class Vehicle(object):
    def __init__(self, x, y, heading, velocity=0.0, wheel_base=2.7):
        self._x = x
        self._y = y
        self._heading = heading
        # m/s
        self._velocity = velocity
        # meter
        self._wheel_base = wheel_base

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def heading(self):
        return self._heading

    @property
    def velocity(self):
        return self._velocity

    @property
    def wheel_base(self):
        return self._wheel_base

    def get_location(self):
        return Point(self._x, self._y)

    def update_status(self, x, y, heading, velocity=0):
        self._x = x
        self._y = y
        self._heading = heading
        self._velocity = velocity

    def __repr__(self):
        return 'Vehicle [x: %.6f, y: %.6f, heading(degree): %.6f]' % \
            (self._x, self._y, _radian_to_degree(self._heading))


class VirtualVehicle(Vehicle):
    def __init__(self, x, y, heading):
        super(VirtualVehicle, self).__init__(x, y, heading)
        self._velocity_weight = 0.5

    def move_forward(self, target_velocity, steering_angle, time_delta):
        self._x += self._velocity * math.cos(self._heading) * time_delta
        self._y += self._velocity * math.sin(self._heading) * time_delta
        self._heading += self._velocity / \
            self._wheel_base * -math.tan(steering_angle) * time_delta

        # PID control to get accelation
        accelation = self._velocity_weight * (target_velocity - self._velocity)
        self._velocity += accelation * time_delta


class RecordedVehicle(Vehicle):
    def __init__(self, records, wheel_base):
        """
        @param records: List of (x, y, heading) values.
        @param wheel_base: Wheel base value of recoding vehicle

        """
        if not records:
            raise Exception('Records cannot be empty')

        self._records = iter(records)
        x, y, heading = next(self._records)
        super(RecordedVehicle, self).__init__(x, y, heading)

    def move_forward(self):
        try:
            self.update_status(*next(self._records))
        except StopIteration:
            # Do not move on the last point if there is no remaining records.
            pass


class Point(object):
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    def to_tuple(self):
        return (self._x, self._y)

    def distance_to(self, other):
        return math.hypot(self._x - other.x, self._y - other.y)

    def __repr__(self):
        return 'Point [%s, %s]' % (self._x, self._y)


class MapPoint(Point):
    def __init__(self, idx, x, y):
        super(MapPoint, self).__init__(x, y)
        self._idx = idx

    @property
    def idx(self):
        return self._idx


class Vector(object):
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @staticmethod
    def from_points(from_, to):
        return Vector(to.x - from_.x, to.y - from_.y)

    @property
    def size(self):
        return math.hypot(self._x, self._y)

    def get_resized_vector(self, target_size):
        vector_size = self.size
        if vector_size == 0:
            raise Exception('Two identical map points detected')

        x, y = self._x, self._y
        # Normalize and extend
        x = x / vector_size * target_size
        y = y / vector_size * target_size
        return Vector(x, y)

    def __add__(self, other):
        return Vector(self._x + other.x, self._y + other.y)

    def __sub__(self, other):
        return Vector(self._x - other.x, self._y - other.y)

    def __eq__(self, other):
        return self._x == other.x and self._y == other.y


class SpatialUtils(object):
    @staticmethod
    def perpendicular_intersection(point, line_start, line_end):
        # We need another point which is on a line which is perpendicular with
        # the line (line_start - line_end) to solve this problem using
        # Cramers's rule. We get this point by moving 1 from the given point.
        if line_start.x == line_end.x:
            another_point = Point(point.x + 1, point.y)
        elif line_start.y == line_end.y:
            another_point = Point(point.x, point.y + 1)
        else:
            slope = (line_end.y - line_start.y) / (line_end.x - line_start.x)
            # Get perpendicular slope
            slope = -1.0 / slope
            intercept = point.y - slope * point.x
            another_x = point.x + 1
            another_y = slope * another_x + intercept
            another_point = Point(another_x, another_y)

        # Now we have 4 points. Let's use cramer's rule.
        # See https://stackoverflow.com/a/20679579/2920110
        def _get_line_coefficients(p1, p2):
            # Returns (a, b, c) from linear system ax + by = c
            a = p1.y - p2.y
            b = p2.x - p1.x
            c = p1.x * p2.y - p2.x * p1.y
            return a, b, -c

        c1 = _get_line_coefficients(point, another_point)
        c2 = _get_line_coefficients(line_start, line_end)

        determinant = c1[0] * c2[1] - c1[1] * c2[0]
        if determinant == 0:
            # This can't be 0 because two lines should have an intersection
            # point.
            raise Exception('Two identical map points detected')

        determinant_x = c1[2] * c2[1] - c1[1] * c2[2]
        determinant_y = c1[0] * c2[2] - c1[2] * c2[0]
        return Point(determinant_x / determinant, determinant_y / determinant)

    @staticmethod
    def is_point_on_line_segment(point, line_start, line_end):
        point_to_ends = \
            point.distance_to(line_start) + point.distance_to(line_end)
        line_len = line_start.distance_to(line_end)
        epsilon = 0.1
        return abs(point_to_ends - line_len) <= epsilon


class KDTree(object):
    def __init__(self, map_points):
        self._map_points = map_points
        # NlogN tree construction
        self._tree_impl = spatial.KDTree([x.to_tuple() for x in map_points])

    def get_nearest_map_point(self, x):
        # logN search
        # distance, idx
        _, idx = self._tree_impl.query(x.to_tuple())

        return self._map_points[idx]


class PurePursuitController(object):
    """
    This implements
    'Implementation of the Pure Pursuit Path Tracking Algorithm'
    RC Conlter, 1992

    """
    def __init__(self, path_points, lookahead_distance = 8.0):
        self._current_goal_point = None
        self._track_idx = 0
        self._lookahead_distance = lookahead_distance
        self._steer_angle = 0.

        self.set_path_points(path_points)

    @property
    def current_goal_point(self):
        return self._current_goal_point

    @property
    def is_available_path(self):
        return len(self._path_points) > 1

    @property
    def path_points(self):
        return self._path_points

    @property
    def steer_angle(self):
        return self._steer_angle
    
    @steer_angle.setter
    def steer_angle(self, steer_angle):
        self._steer_angle = steer_angle
    
    @path_points.setter
    def path_points(self, path_points):
        self._path_points = path_points

    def set_path_points(self, path_points):
        self._path_points = path_points
        self._kdtree = KDTree(path_points)

    def _get_next_goal_point(self, vehicle):
        # Find a point which is closest to the current vehicle position
        vehicle_point = vehicle.get_location()
        nearest_path_point = \
            self._kdtree.get_nearest_map_point(vehicle_point)

        cur_idx = nearest_path_point.idx
        next_map_point = None

        min_d = 1000.
        min_idx = cur_idx
        if abs(self._track_idx - cur_idx) > 5:
            for p_idx in range(len(self._path_points)):
                d = vehicle_point.distance_to(self._path_points[p_idx])

                if d < min_d and p_idx != cur_idx \
                    and abs(self._track_idx - p_idx) < 5:

                    min_d = d
                    min_idx = p_idx

            cur_idx = min_idx

        self._track_idx = cur_idx

        if cur_idx < len(self._path_points) - 1:
            if 0 < cur_idx:
                # We need to determine closest line segment here because
                # choosing closest point alone has some exceptional cases.
                first_seg_a = self._path_points[cur_idx - 1]
                first_seg_b = self._path_points[cur_idx]
                first_intersection = SpatialUtils.perpendicular_intersection(
                    vehicle_point, first_seg_a, first_seg_b
                )

                # If the vehicle is on first_segment, we shouldn't proceed to
                # next nearest point.
                if SpatialUtils.is_point_on_line_segment(
                        first_intersection, first_seg_a, first_seg_b):
                    cur_idx -= 1

            cur_map_point = self._path_points[cur_idx]
            next_map_point = self._path_points[cur_idx + 1]
            cur_point = SpatialUtils.perpendicular_intersection(
                vehicle_point, cur_map_point, next_map_point
            )

            # If cur point is not on the line segment, use cur_map_point as a
            # starting point to ensure that a goal point is always on the map.
            # XXX: This can handle wrong input by preveting goal point from
            # jiggling.
            if not SpatialUtils.is_point_on_line_segment(
                    cur_point, cur_map_point, next_map_point):
                cur_point = cur_map_point
        else:
            cur_point = self._path_points[-1]

        remaining_distance = self._lookahead_distance

        while cur_idx < len(self._path_points) - 1:
            next_map_point = self._path_points[cur_idx + 1]

            distance = cur_point.distance_to(next_map_point)
            if remaining_distance <= distance:
                break

            remaining_distance -= distance
            cur_idx += 1
            cur_point = self._path_points[cur_idx]

        if cur_idx == len(self._path_points) - 1:
            # Last map point reached, return last map point.
            return Point(*self._path_points[-1].to_tuple())

        if not next_map_point:
            return

        next_point = Point(*next_map_point.to_tuple())

        # Move more on the line
        vector = Vector.from_points(cur_point, next_point)
        vector = vector.get_resized_vector(remaining_distance)
        goal_point = Point(
            cur_point.x + vector.x, cur_point.y + vector.y)

        return goal_point

    def _calculate_next_steering_angle(self, vehicle, goal_point):
        """
        See http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6987787
        for a detailed explanation of the formulas in this method.

        """

        # Let's assume that the vehicle only moves forward
        
        theta_vehicle = (vehicle.heading + 2*math.pi) % (2*math.pi)
        theta_goal = math.atan2(
            goal_point.y - vehicle.y,
            goal_point.x - vehicle.x
        )

        # print('origin : ', vehicle.heading, theta_goal, vehicle.heading-theta_goal)

        theta_goal = (theta_goal + 2*math.pi) % (2*math.pi)
        diff_angle = theta_vehicle - theta_goal

        if abs(diff_angle) >= math.pi:
            # print('new : ', theta_vehicle, theta_goal, diff_angle, - np.sign(diff_angle)*2*math.pi, diff_angle - np.sign(diff_angle)*2*math.pi)
            diff_angle = diff_angle - np.sign(diff_angle)*2*math.pi

        k = 1.2
        diff_angle *= k
        diff_angle = diff_angle if abs(diff_angle) <= math.pi else np.sign(diff_angle)*math.pi

        alpha = diff_angle

        # alpha = vehicle.heading - math.atan2(
        #     goal_point.y - vehicle.y,
        #     goal_point.x - vehicle.x
        # )

        # print('alpha, alpha*k, result', alpha, alpha*k, (k*((alpha + 2*math.pi) % (2*math.pi))))
        # print('sin alpha, alpha*k, result', math.sin(alpha), math.sin(alpha*k), math.sin(k*((alpha + 2*math.pi) % (2*math.pi))))
        # alpha = (k*((alpha + 2*math.pi) % (2*math.pi)))
        curvature = 2.0 * vehicle.wheel_base * \
            math.sin(alpha) / self._lookahead_distance
        delta = math.atan2(curvature, 1.0)
        return delta

    def tick(self, vehicle):
        try:
            goal_point = self._get_next_goal_point(vehicle)
        except IndexError as e:
            print(e)
            pass

        if not goal_point:
            return None, 0.

        steer_cmd = self._calculate_next_steering_angle(vehicle, goal_point)

        steer_cmd = steer_cmd if abs(steer_cmd) < 0.546 else np.sign(steer_cmd)*0.546

        # Clipping steering wheel command
        steer_ratio = 16.
        cur_steer = -np.deg2rad(self._steer_angle)/steer_ratio
        if abs(steer_cmd - cur_steer ) > 0.0039:
            steer_cmd += np.sign(steer_cmd - cur_steer)*0.0039

        return goal_point, steer_cmd

    def set_adpative_lookahead_distance(self, velocity):
        if ( velocity < 7 ):
            self._lookahead_distance = 5
        elif ( velocity >= 7 and velocity < 15 ):
            self._lookahead_distance = 0.5*velocity+1.5
        # if ( velocity < 15 ):
        #     self._lookahead_distance = 9
        elif ( velocity >= 15 and velocity < 40 ):
            self._lookahead_distance = 0.6*velocity
        else:
            self._lookahead_distance = 24



def _radian_to_degree(radian):
    return radian * 57.29577


def run_simulation(
        map_points=None, vehicle=None, lookahead_distance=2.0,
        debug_print=False):
    import numpy as np
    # secs
    time_delta = 0.1
    final_goal_epsilon = 1.0

    if map_points is None:
        # 1. Create simulation map points
        xs = np.arange(0, 50, 0.1)
        ys = [math.sin(x / 5.0) * x / 2.0 for x in xs]
        map_points = [MapPoint(
            i, float(xs[i]), ys[i]) for i in range(len(xs))]
        final_goal = map_points[-1]
    else:
        # 1. Parse given map_points
        if not map_points:
            raise Exception('Map points should not be empty')
        xs, ys = zip(*[x.to_tuple() for x in map_points])

    final_goal = map_points[-1]

    if vehicle is None:
        # 2. Create vehicle state
        x, y, heading = 0.0, -3.0, 0.0
        vehicle = VirtualVehicle(x, y, heading)

    # 3. Create history lists for animation
    x_histories = [vehicle.x]
    y_histories = [vehicle.y]

    # 4. Start simulation
    controller = PurePursuitController(
        map_points, lookahead_distance=lookahead_distance)
    while True:
        goal_point, steering_angle = controller.tick(vehicle)

        if isinstance(vehicle, VirtualVehicle):
            # To convert km/h to m/s, devide it by 3.6
            target_velocity = 50.0 / 3.6
            vehicle.move_forward(target_velocity, steering_angle, time_delta)
        elif isinstance(vehicle, RecordedVehicle):
            vehicle.move_forward()
        else:
            raise NotImplementedError

        if debug_print:
            print('-----------------------')
            print('Steering angle (degree)')
            # Convert radian to degree
            print(_radian_to_degree(steering_angle))
            print('Vehicle position')
            print(vehicle)

        # Check if we has arrived at final goal
        straight_distance = final_goal.distance_to(vehicle.get_location())
        if straight_distance <= final_goal_epsilon:
            break

        x_histories.append(vehicle.x)
        y_histories.append(vehicle.y)

        # Start drawing graph
        # 1. Clear graph
        plt.cla()
        # 2. Draw map
        red_point = '.r'
        plt.plot(xs, ys, red_point, label='course')
        # 3. Draw trajectory
        blue_line = '-b'
        plt.plot(x_histories, y_histories, blue_line, label='trajectory')
        # 4. Draw goal point
        green_x_mark = 'xg'
        plt.plot(goal_point.x, goal_point.y, green_x_mark, label='goal_point')
        plt.axis('equal')
        plt.grid(True)
        # * 3.6 to convert m/s velocity to km/h velocity
        plt.title("Speed[km/h]:" + str(vehicle.velocity * 3.6)[:4])
        plt.pause(0.001)


if __name__ == '__main__':
    run_simulation()
