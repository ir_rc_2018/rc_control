#!/usr/bin/env python

import ast
import numpy as np
import copy

import rospy
import tf

from control_msgs.msg import GlobalPath, VehicleState
from std_msgs.msg import Float32, Int8, Bool, String, Float32MultiArray
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped,Twist
from geometry_msgs.msg import Point as ROSPoint
from sensor_msgs.msg import JointState
from visualization_msgs.msg import Marker
from datareader import get_path
from purepursuit import PurePursuitController, Vehicle
from sensor_msgs.msg import NavSatFix, Imu, JointState

CONTROL_PER_SEC = 10

# for purepursit
LOOKAHEAD_DISTANCE = 5
wheel_base = 0.3
steer_ratio = 1.5
x, y, heading = 0., 0., 0.
count_of_long_history = 0
global_vehicle = Vehicle(x, y, heading, wheel_base=wheel_base)
global_path = GlobalPath()

path_follower = PurePursuitController(
    get_path([[0,0]]),
    lookahead_distance=LOOKAHEAD_DISTANCE
)

pub_is_arrive_to_goal = rospy.Publisher("/vehicle_stop_at_goal", Bool, queue_size=1)
pub_cmd = rospy.Publisher("/cmd_vel", Twist, queue_size=1)

autonomous_status = "initial_mode"
prev_autonomous_status = "initial_mode"



goal_position = []
gps_enable = True

RSSI = 10000
RSSI_history = [] 		# 1 sec period
RSSI_long_history = [] 	# 10 sec period
moving_average =[]

turn_integration = 0
on_turn = False
go_integration   = 0
on_go = False
start_stop_time = 0

count_of_check_RSSI = 0

distances_ultra = []
MAX_D = 10000.

def update_vehicle_status(msg):

    global_vehicle.update_status(
        msg.x,
        msg.y,
        msg.heading
    )
    # adaptive lookahead distance setting
    #if speed_decider.cur_link_on_path == 4 or need_stabilization:
    #    path_follower.set_adpative_lookahead_distance(msg.v_ego)
    #else:
    #    path_follower.set_adpative_lookahead_distance(msg.v_ego if msg.v_ego >= 15 else 15)	
    
    path_follower.steer_angle = msg.steer_angle


def update_vehicle_status_test(x,y,heading):

    global_vehicle.update_status(
        x,
        y,
        heading
    )
    # adaptive lookahead distance setting
    #if speed_decider.cur_link_on_path == 4 or need_stabilization:
    #    path_follower.set_adpative_lookahead_distance(msg.v_ego)
    #else:
    #    path_follower.set_adpative_lookahead_distance(msg.v_ego if msg.v_ego >= 15 else 15)	
    
    path_follower.steer_angle = 0. #msg.steer_angle

# for simulation in rviz
def callback_cur_position(msg):

	heading = tf.transformations.euler_from_quaternion(\
	                        (msg.pose.pose.orientation.x,
	                        msg.pose.pose.orientation.y,
	                        msg.pose.pose.orientation.z,
	                        msg.pose.pose.orientation.w)
	                    )[2]
	
	global_vehicle.update_status(
		msg.pose.pose.position.x,
 		msg.pose.pose.position.y,
		heading,
	)
	update_vehicle_status_test(global_vehicle.x, global_vehicle.y, global_vehicle.heading)


# for mpc test


def update_gps_enable(msg):
	global gps_enable
	gps_enable = msg.data




def callback_global_path(path):
	global global_path, goal_position
	global_path = copy.deepcopy(path)

	if len(global_path.path_x) < 2:
		return

	if len(global_path.path_x) > 0:
		path_follower.set_path_points(
			get_path(zip(global_path.path_x, 
						global_path.path_y)))
	else:
		print("Can not find any path ")

	goal_position = [global_path.path_x[-1], global_path.path_y[-1]]


	# print('received global_path')

	# speed_decider.path = copy.deepcopy(global_path)
	# speed_decider.set_traffic_info(traffic_rules)

	# path_follower.set_path_points(
	# 	get_path(zip(global_path.path_x, 
	# 				global_path.path_y)))

	# print('len path_x, path_y : ', len(global_path.path_x), len(global_path.path_y))

	# published_is_arrive_to_goal = False
	# is_inserted_path = True

def callback_imu(data) :
	global turn_integration
	#vehicle_state.yaw_rate = data.angular_velocity.z
	if on_turn == True :
		turn_integration = turn_integration + np.rad2deg(data.angular_velocity.z) / 150.
		#print '[%.2f , %.2f] : turn_integration, yaw rate'%(turn_integration,data.angular_velocity.z)
	else :
		turn_integration = 0

def callback_vehicle_speed(data):
	global go_integration

	wheel_R = 0.033 # [m]
	v_ego = (data.velocity[0]+data.velocity[1])/(2*np.pi)*(2*np.pi*wheel_R)/2

	if on_go == True :
		go_integration = go_integration + v_ego / 30.
	else :
		go_integration = 0



def go(meter) :
	# input  : how much robot go
	# output : cmd, how much robot went
	global on_go, go_integration

	print '[%.2f, %.2f] : go_integration , meter'%(go_integration , meter)
	if go_integration > meter :
		on_go = False
		go_integration = 0
		cmd = Twist()
		return cmd , meter

	else :
		on_go = True
		cmd = Twist()
		cmd.linear.x = 0.08
		return cmd , go_integration

def turn(degree) :
	global on_turn, turn_integration

	print '[%.2f, %.2f] : turn_integration , degree'%(turn_integration , degree)
	if abs(turn_integration) > abs(degree) :
		on_turn = False 
		turn_integration = 0
		cmd = Twist()
		return cmd , degree
	else :
		on_turn = True
		cmd = Twist()
		cmd.angular.z = np.sign(degree) * 0.5
		return cmd, turn_integration


def stop(duration) :
	global start_stop_time

	if start_stop_time == 0 :
		start_stop_time = rospy.get_time()

	how_stop = abs(rospy.get_time() - start_stop_time)
	if how_stop < duration :
		cmd = Twist()
		return cmd , how_stop
	else :
		start_stop_time = 0
		cmd = Twist()
		return cmd, how_stop


def check_RSSI(history_RSSI) :
	# return 'is it good direction?'
	global count_of_check_RSSI

	offset = -4
	if len(history_RSSI) != 5 :
		return True

	cur_RSSI = history_RSSI[-1]
	print 'check_RSSI_history', history_RSSI
	invalid_count = 0
	for idx in range(len(history_RSSI)-1) :
		if cur_RSSI < history_RSSI[idx] + offset :
			invalid_count = invalid_count + 1

	if invalid_count >= 2 :
		count_of_check_RSSI = count_of_check_RSSI + 1
		if count_of_check_RSSI > 29 :
			count_of_check_RSSI = 0
			return False
		else :
			return True
	else :
		count_of_check_RSSI = 0
		return True


def simple_check_RSSI(history_RSSI) :
	# return 'is it good direction?'

	offset = -2
	if len(history_RSSI) != 5 :
		return True

	cur_RSSI = history_RSSI[-1]
	print 'simple_check_RSSI_history', history_RSSI
	invalid_count = 0
	for idx in range(len(history_RSSI)-1) :
		if cur_RSSI < history_RSSI[idx] + offset :
			invalid_count = invalid_count + 1

	if invalid_count >= 2 :
		return False

	else :
		return True


def callback_wifi_rssi(msg) :
	global RSSI, RSSI_history, RSSI_long_history, count_of_long_history

	rssi = msg.data
	#print '[%.2f] : rssi data'%(rssi)
	if rssi < 0 :
		if len(moving_average) < 5 :
			moving_average.append(rssi)
		else :
			moving_average.pop(0)
			moving_average.append(rssi)
	
		if len(moving_average) == 5 :
			RSSI = np.dot(moving_average,[0.1, 0.1, 0.1, 0.3, 0.4])
		else :
			RSSI = np.mean(moving_average)

		# RSSI history part
		if len(RSSI_history) < 5 :
			RSSI_history.append(RSSI)
		else :
			RSSI_history.pop(0)
			RSSI_history.append(RSSI)

		# RSSI history part
		count_of_long_history = count_of_long_history + 1
		if count_of_long_history % 10 == 0 :
			if len(RSSI_long_history) < 5 :
				RSSI_long_history.append(RSSI)
			else :
				RSSI_long_history.pop(0)
				RSSI_long_history.append(RSSI)



def reset_wifi_mode() :
	global start_stop_time, on_go, go_integration, on_turn, turn_integration

	start_stop_time = 0
	on_go = False
	go_integration = 0
	on_turn = False
	turn_integration = 0

def reset_ultrasonic_mode_transient():
	reset_wifi_mode()


def callback_ultrasonic(msg) :
	global distances_ultra
	

	if len(msg.data) == 1 :
		distances_ultra = [MAX_D, msg.data[0]*0.01, MAX_D]
	elif len(msg.data) == 3:
		distances_ultra = np.multiply(msg.data, 0.01)
	else :
		print 'callback_ultrasonic data error'
		return 







def run_node():
	global pub_cmd, prev_autonomous_status, RSSI_history, RSSI_long_history

	rospy.set_param('/debug/rc_mode', 'initial_mode')

	rospy.init_node('control_planner')

	rospy.Subscriber('/vehicle_state', VehicleState, update_vehicle_status)
	rospy.Subscriber('/global_path', GlobalPath, callback_global_path)
	rospy.Subscriber('/gps_enable', Bool, update_gps_enable)
	rospy.Subscriber('/joint_states',JointState, callback_vehicle_speed)

	# for simulation in rviz
	rospy.Subscriber("/initialpose", PoseWithCovarianceStamped, callback_cur_position)
	rospy.Subscriber('/wifi_rssi', Float32, callback_wifi_rssi)
	rospy.Subscriber("/imu", Imu, callback_imu)



	rospy.Subscriber('/distance_from_ultra', Float32MultiArray, callback_ultrasonic)
			# fix speed, move 


	wifi_state = 0 	# 0 : go 2 m & check
					# 1 : turn 120 degree
					# 2 : go 1 m 
					# 3 : stop 5 seconds & check
					# 4 : turn 150 degree  / next state get 0
	on_wifi_mode = False
	rssi_strength_of_near_target = -28
	prev_RSSI = 0

	on_ultrasonic_mode = False
	ultrasonic_state = 0
	prev_ultrasonic_state = 0
	min_D = MAX_D


	rate = rospy.Rate(CONTROL_PER_SEC)

	while not rospy.is_shutdown():
		

		# update by purepursuit
		steer_cmd = 0.0

		# update target speed by mpc and speed decider
		target_speed = 0.0
		feed_accel = 0.0

		cmd = Twist()
		autonomous_status = rospy.get_param('/debug/rc_mode', 'initial_mode')


		cur_P = np.array([global_vehicle.x, global_vehicle.y])
		goal_P = np.array(goal_position)
		distance_to_goal = 100.
		if  len(goal_P) != 0 :
			distance_to_goal = np.sqrt(np.sum((cur_P-goal_P)**2))

		print '[%.2f ] :distance_to_goal'%(distance_to_goal)			

		# test condition 
		# on_wifi_mode = True




		if autonomous_status == "gps_mode" :

			if ( path_follower.is_available_path ) :
				# print('speed_decider.cur_link_on_path : ', speed_decider._cur_link_idx_on_path)
				_, steer_cmd = path_follower.tick(global_vehicle)
				print('path path_follower steer_cmd : ', -np.rad2deg(steer_cmd))#-np.rad2deg(steer_cmd)*steer_ratio)


			steer_angle_cmd = -steer_cmd*steer_ratio# [rad/s] #-np.rad2deg(steer_cmd)*steer_ratio


			cmd.linear.x = 0.08 # [m/s]
			cmd.angular.z = steer_angle_cmd

			# end condition of gps

			if distance_to_goal < 0.5 :
				cmd = Twist()
				print 'find_goal'


		elif autonomous_status == "wifi_mode" :
			# gps + wifi mode
			valid_threshold_RSSI = -60

			if ( path_follower.is_available_path ) :
				# print('speed_decider.cur_link_on_path : ', speed_decider._cur_link_idx_on_path)
				_, steer_cmd = path_follower.tick(global_vehicle)
				print('path path_follower steer_cmd : ', -np.rad2deg(steer_cmd))#-np.rad2deg(steer_cmd)*steer_ratio)


			steer_angle_cmd = -steer_cmd*steer_ratio# [rad/s] #-np.rad2deg(steer_cmd)*steer_ratio


			cmd.linear.x = 0.08 # [m/s]
			cmd.angular.z = steer_angle_cmd
			# fix speed, move 

			# valid RSSI region

			if len(RSSI_history) == 5 : 
										# add condition or whether position accuracy of GPS were higher than 300
										#							in this case, make gps unable
										# 				or whether the rssi get worse.
				print 'gps_enable',gps_enable
				if gps_enable == False or ( wifi_state == 0 and check_RSSI(RSSI_long_history) == False ):
					on_wifi_mode = True

				if np.mean(RSSI_history[-3:]) > rssi_strength_of_near_target :
					on_wifi_mode = False
					reset_wifi_mode()

				
				if on_wifi_mode == True :
					#if wifi_state == 0 and check_RSSI(RSSI_history) == False :
					#	prev_RSSI = RSSI_history[-1]
					#	wifi_state = 1

					if RSSI_history[-1] >  valid_threshold_RSSI :


						if wifi_state == 0 :
							cmd , how_go = go(1)
							if how_go < 0.05 :
								prev_RSSI = RSSI_history[-1]
							if how_go >= 1 :
								print '[%.2f, %.2f] : prev_RSSI, RSSI_history[-1]'%(prev_RSSI, RSSI_history[-1])
								if prev_RSSI < 0 and prev_RSSI > RSSI_history[-1]:
									wifi_state = 1
									prev_RSSI = RSSI_history[-1]
								else :
									wifi_state = 0
								

						elif wifi_state == 1 :
							cmd, how_turn = turn(90)
							if how_turn < 90 :
								wifi_state = 1
							else :
								wifi_state = 2

						elif wifi_state == 2 :
							cmd, how_go = go(0.7)
							if how_go < 0.7 :
								wifi_state = 2
							else :
								wifi_state = 3

						elif wifi_state == 3 :
							cmd, how_stop = stop(5)
							if how_stop < 5 :
								wifi_state = 3
							else :
								cur_RSSI = np.mean(RSSI_history)
								if cur_RSSI > prev_RSSI :
									wifi_state = 0
								else :
									wifi_state = 4

						elif wifi_state == 4 :
							cmd, how_turn = turn(180)
							if how_turn < 180 :
								wifi_state = 4 
							else :
								wifi_state = 5
						elif wifi_state == 5 :
							cmd , how_go = go(1.4)
							if how_go < 1.4 :
								wifi_state = 5
							else :
								wifi_state = 6


						elif wifi_state == 6 :
							cmd, how_stop = stop(5)
							if how_stop < 5 :
								wifi_state = 6
							else :
								cur_RSSI = np.mean(RSSI_history)
								if cur_RSSI > prev_RSSI :
									wifi_state = 7
								else :
									wifi_state = 6
								prev_RSSI = cur_RSSI


						elif wifi_state == 7 :
							cmd, how_turn = turn(90)
							if how_turn < 90 :
								wifi_state = 7 
							else :
								wifi_state = 0



			if on_wifi_mode == False :
				cmd = Twist()
			print '[%r ,%d, %.2f] : on_wifi_mode, wifi_state, RSSI'%(on_wifi_mode, wifi_state, RSSI)


		elif autonomous_status == "ultrasonic_mode" :
			# gps + wifi mode
			valid_threshold_RSSI = -60

			if ( path_follower.is_available_path ) :
				# print('speed_decider.cur_link_on_path : ', speed_decider._cur_link_idx_on_path)
				_, steer_cmd = path_follower.tick(global_vehicle)
				print('path path_follower steer_cmd : ', -np.rad2deg(steer_cmd))#-np.rad2deg(steer_cmd)*steer_ratio)


			steer_angle_cmd = -steer_cmd*steer_ratio# [rad/s] #-np.rad2deg(steer_cmd)*steer_ratio


			cmd.linear.x = 0.08 # [m/s]
			cmd.angular.z = steer_angle_cmd
			# fix speed, move 

			# valid RSSI region



			if len(RSSI_history) == 5 : 
				print 'gps_enable',gps_enable
				
				if gps_enable == False or ( wifi_state == 0 and check_RSSI(RSSI_long_history) == False and np.mean(RSSI_history[-3:]) > -50.) or \
				distance_to_goal < 1.:
					if on_ultrasonic_mode == False :
						on_wifi_mode = True

				if np.mean(RSSI_history[-3:]) > rssi_strength_of_near_target :
					on_wifi_mode = False
					reset_wifi_mode()
					# test
					on_ultrasonic_mode = True

				
				if on_wifi_mode == True :
					#if wifi_state == 0 and check_RSSI(RSSI_history) == False :
					#	prev_RSSI = RSSI_history[-1]
					#	wifi_state = 1

					if RSSI_history[-1] >  valid_threshold_RSSI :


						if wifi_state == 0 :
							cmd , how_go = go(1)
							if how_go < 0.05 :
								prev_RSSI = RSSI_history[-1]
							if how_go >= 1 :
								print '[%.2f, %.2f] : prev_RSSI, RSSI_history[-1]'%(prev_RSSI, RSSI_history[-1])
								if prev_RSSI < 0 and prev_RSSI > RSSI_history[-1]:
									wifi_state = 1
									prev_RSSI = RSSI_history[-1]
								else :
									wifi_state = 0
								

						elif wifi_state == 1 :
							cmd, how_turn = turn(45)
							if how_turn < 45 :
								wifi_state = 1
							else :
								wifi_state = 2

						elif wifi_state == 2 :
							cmd, how_go = go(1)
							if how_go < 1 :
								wifi_state = 2
							else :
								wifi_state = 3

						elif wifi_state == 3 :
							cmd, how_stop = stop(5)
							if how_stop < 5 :
								wifi_state = 3
							else :
								cur_RSSI = np.mean(RSSI_history)
								if cur_RSSI > prev_RSSI :
									wifi_state = 0
								else :
									wifi_state = 4

						elif wifi_state == 4 :
							cmd, how_turn = turn(-90)
							if abs(how_turn) < 90 :
								wifi_state = 4 
							else :
								wifi_state = 5
						elif wifi_state == 5 :
							cmd , how_go = go(1)
							if how_go < 1 :
								wifi_state = 5
							else :
								wifi_state = 6


						elif wifi_state == 6 :
							cmd, how_stop = stop(5)
							if how_stop < 5 :
								wifi_state = 6
							else :
								cur_RSSI = np.mean(RSSI_history)
								if cur_RSSI > prev_RSSI :
									wifi_state = 7
								else :
									wifi_state = 6
								prev_RSSI = cur_RSSI


						elif wifi_state == 7 :
							cmd, how_turn = turn(90)
							if how_turn < 90 :
								wifi_state = 7 
							else :
								wifi_state = 0


			if on_ultrasonic_mode == True :
				# check 3 ultra sonic sensors. to be continue...	
				# if one measurement of them were within 2m, stop turn and go theses direction.
				# if all measurements exceed 2m, turn.
				# if it were in 50cm, stop. it mean we found target.
				ultrasonic_state = 0# -1: turn -360  
									# 0 : turn 360 - finding object by turnning
									# 1 : distance of left ultra is min
									# 2 : distance of center ultra is min
									# 3 : distance of right ultra is min
									# 10: found goal.

				if ultrasonic_state != prev_ultrasonic_state :
					reset_ultrasonic_mode_transient()

				prev_ultrasonic_state = ultrasonic_state

				if len(distances_ultra) != 3 :
					cmd = Twist()
				else :

					min_D = np.min(distances_ultra)
					if min_D <= 2. :
						min_index = np.argmin(distances_ultra)

						if min_index   == 0 :
							ultrasonic_state = 1

						elif min_index == 1 :
							ultrasonic_state = 2

						elif min_index == 2 :
							ultrasonic_state = 3

						else :
							ultrasonic_state = 0

					else :
						if prev_ultrasonic_state == 3 :
							ultrasonic_state = -1 
						else :
							ultrasonic_state = 0

					if min_D <= 0.5 :
						ultrasonic_state = 10
						print 'find_goal!!'


					if ultrasonic_state == -1 :
						cmd, how_turn = turn(-360)

					elif ultrasonic_state == 0 :
						cmd, how_turn = turn(360)

					elif ultrasonic_state == 1 :
						cmd , how_turn = turn(30)
						if how_turn >= 30 :
							ultrasonic_state = 2 

					elif ultrasonic_state == 2 :
						cmd , how_go   = go(0.5)

					elif ultrasonic_state == 3 :
						cmd , how_turn = turn(-30)
						if abs(how_turn) >= 30 :
							ultrasonic_state = 0

					else :
						cmd = Twist()


			if distance_to_goal < 0.3 :
				cmd = Twist()
				print 'find_goal_end'




			if on_wifi_mode == True :
				print '[%r ,%d, %.2f] : on_wifi_mode, wifi_state, RSSI'%(on_wifi_mode, wifi_state, RSSI)
			elif on_ultrasonic_mode == True :
				print '[%r , %.2f, %.2f] : on_ultrasonic_mode, ultrasonic_state, min_distance'%(on_ultrasonic_mode, ultrasonic_state, min_D) 
			else :
				print 'on gps_mode'



			if gps_enable == False and on_wifi_mode == False and on_ultrasonic_mode == False:
				cmd = Twist()

			#print 'in ultrasonic mode'
		else :
			cmd = Twist()


		if autonomous_status == 'gps_mode' or autonomous_status == 'wifi_mode' or autonomous_status == 'ultrasonic_mode' :
			pub_cmd.publish(cmd)

		# In mode transition, set velocity as 0.
		if autonomous_status != prev_autonomous_status :
			cmd = Twist()
			pub_cmd.publish(cmd)

		prev_autonomous_status = autonomous_status

		rate.sleep()

if __name__ == '__main__':
    run_node()
