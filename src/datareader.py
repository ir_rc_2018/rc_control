import ast

from purepursuit import MapPoint


def _set_precision(x, precision=3):
    return int(x * (10 ** precision)) / float((10 ** precision))


def get_path(points):

    for i in range(len(points)):
        point = points[i]
        points[i] = (_set_precision(point[0]), _set_precision(point[1]))

    visited = {}
    # Remove duplicated point
    unique_map_points = []
    idx = 0
    for point in points:
        if str(point) in visited:
            continue
        unique_map_points.append(MapPoint(idx, point[0], point[1]))
        idx += 1
        visited[str(point)] = True

    return unique_map_points